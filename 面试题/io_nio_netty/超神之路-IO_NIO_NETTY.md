[toc]

# Java超神之路-IO_NIO_NETTY

> author：编程界的小学生
>
> date：2021/01/30

# 一、IO

## 1、BufferedOutputStream和FileOutputStream谁快？

buffer快，因为FileOutputStream的是每次witre()都会执行syscall调用内核写入pagecache，而BufferedOutputStream的write()则是每次都写入jvm的8kb大小的缓冲区，等8kb满了后再执行syscall调用系统内核写入pagecache，不会每次都进行系统调用。